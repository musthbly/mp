#!/usr/bin/env dash
# usage: cover target_dir cover_img

err() {
  echo >&2 "$@"
  exit 1
}

if [ ! -d "$1" ]; then
  err "target directory must be specified"
elif [ -z "$2" ]; then
  err "cover image must be specified"
fi

# metadata from directory title (in format: Author - Year - Album)

author() {
  echo "$1" | awk '{split($0, metadata, " - "); print metadata[1]}'
}

year() {
  echo "$1" | awk '{split($0, metadata, " - "); print metadata[2]}'
}

album() {
  echo "$1" | awk '{split($0, metadata, " - "); print metadata[3]}'
}

mkdir "_$1"
cd "$1" || exit

for file in ./*.mp3; do
  ffmpeg -y -loglevel error -i "$file" -i "$2" \
    -map 0:0 -map 1:0 -c copy -id3v2_version 3 \
    -metadata:s:v title="Album cover" \
    -metadata:s:v comment="Cover (front)" \
    "../_$1/$file" || exit
done

cd .. || exit
rm -r "$1"
mv "_$1" "$1"
